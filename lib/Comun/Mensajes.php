<?php 

	namespace Comun;

	/**
	* Clase estatica donde se almacenan todos los mensajes al usuario
	*/
	class Mensajes
	{
		/**
		 * Mensaje de bienvenida al API
		 * @var string
		 */
		public static $API                          = "API Turecorrido.com - Servicios del portal turecorrido.com";

		/**
		 * Mensaje en caso de que al momento de registrar un usuario el nombre de usuario ya este registrado
		 * @var string
		 */
		public static $LOGIN_EXISTE                 = "El nombre de usuario Ya esta Registrado";

		/**
		 * Mensaje en caso de que al momento de registrar un usuario el correo electronico ya exista
		 * @var string
		 */
		public static $CORREO_EXISTE                = "El correo electronico Ya esta Registrado";
		
		/**
		 * Mensaje en caso de que al momento de registrar un usuario no se envie la clave
		 * @var string
		 */
		public static $NO_PASSWORD                  = "Se necesita una clave";

		/**
		 * Mensaje en caso de que el registro del usuario sea exitoso
		 * @var string
		 */
		public static $USUARIO_REGISTRADO           = "Registro Exitoso Bienvenido(a)!!";

		/**
		 * Mensaje en caso de que algo falle al momento de hacer la insercion del usuario
		 * @var string
		 */
		public static $USUARIO_NO_REGISTRADO        = "Error al procesar elregistro, Intente mas tarde";

		/**
		 * Mensaje en al momento de actualizar los datos del usuario
		 * @var string
		 */
		public static $USUARIO_ACTUALIZADO          = "Datos actualizados exitosamente!!";

		/**
		 * Mensaje al momento de actualizar la clave del usuario
		 * @var string
		 */
		public static $USUARIO_CLAVE_ACTUALIZADA    = "Clave actualizada exitosamente!!";

		/**
		 * Mensaje en caso de que no se pueda actualizar la clave del usuario
		 * @var string
		 */
		public static $USUARIO_CLAVE_NO_ACTUALIZADA = "Error al actualizar su clave, intente mas tarde";

	}

?>