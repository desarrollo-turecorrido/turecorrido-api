<?php

	namespace Comun;

	/**
	 * Clase que contiene metodos comunes
	 */
	class Utilitarios
	{
		/**
		* Configuracion del servidor REST
		* @var stdClass
		*/
		private static $config,$usuario;

		/**
		* Almacena la configuración basica del servidor
		* @param stdClass $config StdClass con la configuracion
		*/
		public static function defineConfiguracion($config){

			self::$config = $config;

		}

		/**
		 * Guarda el usuario en memoria
		 * @param  Model\Usuario     $usuario     Instancia del modelo de la tabla usuario con el usuario que se esta conectando
		 * @return boolean                        Devuelve TRUE en caso de que se guarde la instancia del usuario exitosamente
		 */
		public static function defineUsuario($usuario){

			self::$usuario = $usuario;

			return true;

		}

		/**
		 * Devuelve el usuario actual
		 * @return Model\Usuario
		 */
		public static function obtenerUsuario(){

			return self::$usuario;

		}

		/**
		 * Devuelve el puntage que se le otorga a un usuario por regiustrase
		 * @return integer
		 */
		public static function puntosPorRegistro(){

			return self::$config->puntosPorRegistrarse;

		}
	}
?>