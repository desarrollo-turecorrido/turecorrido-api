<?php
    namespace Comun;

    use Slim\Middleware;
    use Modelos\Usuarios as User;
    use Comun\Utilitarios as Util;

 
class HttpBasicAuth extends Middleware
{
    /**
     * @var string
     */
    protected $realm;

    private $usuario,$clave;
 
    /**
     * Constructor
     *
     * @param   string  $realm      The HTTP Authentication realm
     */
    public function __construct($realm = 'Area Restringida')
    {
        global $app;

        //echo "<pre>";var_dump(get_class_methods($app));exit;

        $req           = $app->request();
        $this->realm   = $realm;
        $this->usuario = $req->headers('PHP_AUTH_USER');
        $this->clave   = $req->headers('PHP_AUTH_PW');
    }
 
    /**
     * Funcion que se ejecuta para denegar el acceso
     *
     */   
    public function denegar() {

        $res = $this->app->response();
        $res->body("<h1>Acceso Denegado</h1>");$res->status(401);
        $res->header('WWW-autenticar', sprintf('Basic realm="%s"', $this->realm));
    }
 
    /**
     * autenticar
     *
     * @param   string  $usuario   The HTTP Authentication usuario
     * @param   string  $clave   The HTTP Authentication clave    
     *
     */
    public function autenticar() {//firstByAttributes

        $usuario = User::firstByAttributes(array('usulogin' => $this->usuario,'usupass' => $this->clave));

        return (get_class($usuario)=='Modelos\Usuarios')?Util::defineUsuario($usuario):false;
    }
 
    /**
     * Call
     *
     * Este método comprobará los encabezados de solicitud HTTP para la
     * autenticación previa
     * 
     * SI la solicitud es autenticada, continuara el proceso. De lo contrario, 
     * una respuesta al cliente sera un 401
     * 
     */
    public function call()
    {
        if ($this->autenticar()) {$this->next->call();} 
        else {$this->denegar();}
    }
}