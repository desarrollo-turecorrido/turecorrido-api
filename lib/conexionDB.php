<?php 


use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

/**
 * Diferentes conecciones a la base de datos
 * @var array
 */
$db_conecciones=array(
    'desarrollo'  =>array(
        'driver'    => 'pgsql',
        'host'      => '127.0.0.1',
        'database'  => 'turecorrido',
        'username'  => 'recorriendo',
        'password'  => '123456',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    ),
    'produccion'  =>array(
        'driver'    => 'pgsql',
        'host'      => 'localhost',
        'database'  => 'turecorrido',
        'username'  => 'recorriendo',
        'password'  => '*arsd$$2014*',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => ''
    ),
);

$capsule = new Capsule;
$capsule->addConnection($db_conecciones['desarrollo']);
$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->setAsGlobal();$capsule->bootEloquent();

?>