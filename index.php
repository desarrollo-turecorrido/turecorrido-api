<?php

	require 'vendor/autoload.php';

	use Slim\Slim;
    use Slim\LogWriter;
    use Slim\Log;
    use Comun\Mensajes as MSJ;
    use Servicios\Usuarios;
    use Servicios\Preguntas;
    use Comun\Utilitarios as Util;
    use Comun\HttpBasicAuth;

    Util::defineConfiguracion(json_decode(file_get_contents(dirname(__FILE__).'/app/config.json')));

    $app  = new Slim();

    $app->config(array(

    	'debug' => false,
        'log.enabled' => true,
        'log.writer' => new LogWriter(fopen('app/logs/log', 'a')),
        'log.level' => Log::DEBUG

	));

    $app->add(new HttpBasicAuth);

    function APIrequest(){

        $app = \Slim\Slim::getInstance();

        $app->view(new \JsonApiView());

        $app->add(new \JsonApiMiddleware());
    }

	$app->hook('slim.before', function () use ($app) {});
    
	$app->hook('slim.after.router',function()use ($app){});

    $app->get('/','APIrequest', function() use ($app) {

        $app->render(200,array('msg' => MSJ::$API));
    });

    $app->get('/login','APIrequest', array(new Usuarios,'login'));

    $app->post('/registrar-usuario','APIrequest', array(new Usuarios,'registrar'));

    $app->put('/actualizar-usuario','APIrequest', array(new Usuarios,'actualizar'));

    $app->put('/actualizar-clave-usuario','APIrequest', array(new Usuarios,'actualizarClave'));

    $app->get('/preguntas(/:nivel)','APIrequest', array(new Preguntas,'todas'));



    $app->run();
?>