<?php 
	namespace Modelos;
	use Illuminate\Database\Eloquent\Model;
	/**
	* Modelo para la tabla usuario
	*/
	class Usuarios extends Model{
		
		protected $guarded  = array();

		protected $fillable = array('usulogin','usupass','usucorreo','usutype','usunombres','usuapellidos','puntos','usuregistro');

		public $timestamps = false;

		protected $primaryKey = 'usuid';

	}
?>