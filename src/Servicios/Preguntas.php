<?php

	namespace Servicios;

	use Modelos\Preguntas as Preg;
	use Comun\Utilitarios as Util;

	
	/**
	 * Esta clase maneja todas las acciones de las preguntas
	 */
	class Preguntas{

		public function Todas($nivel = 1 ){

			global $app;

			$preguntas = Preg::where('prenivel' ,'=',$nivel )->get();

    	    $app->render(200,array('data' => !!$preguntas?$preguntas->toArray():array()));
		}
	}
?>