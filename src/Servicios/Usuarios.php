<?php

	namespace Servicios;

	use Modelos\Usuarios as User;
	use Comun\Utilitarios as Util;
	use Comun\Mensajes;

	
	/**
	 * Esta clase maneja todas las acciones de los usuarios
	 */
	class Usuarios{

		/**
		 * Servicio que permite el login del usuario
		 */
		public function login(){

			global $app;

    	    $app->render(200,array('data' => Util::obtenerUsuario()->toArray()));
		}

		/**
		 * Servicio para registrar un usuario
		 */
		public function registrar(){
			
			global $app;

			extract($app->request->post());

			$login  = (bool)User::where('usulogin','=', $usulogin)->count();

			$correo = (bool)User::where('usucorreo','=', $usucorreo)->count();

			if(!!$login){

				$app->render(200,array('msg' => Mensajes::$LOGIN_EXISTE, 'error' => TRUE,'data'=>$app->request->post()));

			}
			else if(!!$correo){

				$app->render(200,array('msg' => Mensajes::$CORREO_EXISTE, 'error' => TRUE,'data'=>$app->request->post()));

			}
			else if(!isset($usupass)){

				$app->render(200,array('msg' => Mensajes::$NO_PASSWORD, 'error' => TRUE,'data'=>$app->request->post()));

			}
			
			$values = array(
				'usulogin'     => $usulogin,
				'usupass'      => $usupass,
				'usutype'      => 'usuario',
				'usucorreo'    => $usucorreo,
				'usunombres'   => isset($usunombres)?usunombres:'',
				'usuapellidos' => isset($usuapellidos)?usuapellidos:'',
				'puntos'       => Util::puntosPorRegistro(),
				'usuregistro'  => 'correo'
			);

			$user = User::create($values);

			$app->render(
				200,
				array(
					'msg'   => !!$user ?Mensajes::$USUARIO_REGISTRADO:Mensajes::$USUARIO_NO_REGISTRADO, 
					'error' => !$user, 
					'data'  => !!user?$user->toArray():array()
				)
			);
		}

		/**
		 * Servicio para actualizar la clave del usuario
		 */
		public function actualizarClave(){

			global $app;

			$usuario = Util::obtenerUsuario();

			extract($app->request->put());

			if($usuario->usupass==$antiguaclave){

				$usuario->update(array('usupass' => $nuevaclave));

				$app->render(200,array('msg' => Mensajes::$USUARIO_CLAVE_ACTUALIZADA, 'data' => $usuario->toArray()));

			}

			$app->render(200,array('msg' => Mensajes::$USUARIO_NO_CLAVE_ACTUALIZADA, 'error' => TRUE));

		}

		/**
		 * Servicio para actualizar los datos del usuario
		 */
		public function actualizar(){

			global $app;

			extract($app->request->put());

			$usuario = Util::obtenerUsuario();

			$values = array(
				'usunombres'   => isset($usunombres)?$usunombres:$usuario->usunombres,
				'usuapellidos' => isset($usuapellidos)?$usuapellidos:$usuario->usuapellidos
			);

			$usuario->update($values);

			$app->render(200,array('msg' => Mensajes::$USUARIO_ACTUALIZADO, 'data' => $usuario->toArray()));
		}
	}
?>